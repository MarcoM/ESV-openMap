# Ecosistema Verbano Open Map
---
Nell'ambito del Mappathon di Piemonte Visual Contest ho georiferito e pubblicato su mappa interattiva molte delle schede di approfondimento sul territotorio del Lago Maggiore prodotte nell'ambito del progetto [EcoSistema Verbano](http://www.ecosistemaverbano.org). Laddove utile e possibile, ho provveduto a riportare la localizzazione di punti di interesse storico/culturale/turistico su OSM, seguendo alcuni criteri:

+ certezza e affidabilità della localizzazione;
+ preferenza degli elementi già presenti in mappa ed arricchimento degli stessi con ulteriori specifiche (es. aggiungendo tag name e description ove mancanti);
+ scelta delle tag in base alle indicazioni del wiki. In casi dubbi, ho sfruttato le discussioni presenti sui vari archivi delle liste OSM;
+ contestuale correzione di alcuni errori o aggiunta di alcuni elementi ulteriori anche non direttamente collegati alla mappatura di Ecosistema Verbano (es. correzione nomi vie, ecc.)

Il risultato finale è consultabile all'indirizzo [eco.nobili.org](eco.nobili.org). 
  
I dati di Ecosistema Verbano sono rilasciati sotto licenza [CC BY-SA-NC](http://creativecommons.org/licenses/by-nc-sa/4.0/deed.it).

