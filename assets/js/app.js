var map, featureList, ev_mestieriSearch = [], ev_acquaSearch = [], ev_memoriaSearch = [], ev_culturaSearch = [];

$(document).on("click", ".feature-row", function(e) {
  sidebarClick(parseInt($(this).attr("id"), 10));
});

$("#about-btn").click(function() {
  $("#aboutModal").modal("show");
  $(".navbar-collapse.in").collapse("hide");
  return false;
});

$("#full-extent-btn").click(function() {
  map.fitBounds(boroughs.getBounds());
  $(".navbar-collapse.in").collapse("hide");
  return false;
});

$("#legend-btn").click(function() {
  $("#legendModal").modal("show");
  $(".navbar-collapse.in").collapse("hide");
  return false;
});

$("#login-btn").click(function() {
  $("#loginModal").modal("show");
  $(".navbar-collapse.in").collapse("hide");
  return false;
});

$("#list-btn").click(function() {
  $('#sidebar').toggle();
  map.invalidateSize();
  return false;
});

$("#nav-btn").click(function() {
  $(".navbar-collapse").collapse("toggle");
  return false;
});

$("#sidebar-toggle-btn").click(function() {
  $("#sidebar").toggle();
  map.invalidateSize();
  return false;
});

$("#sidebar-hide-btn").click(function() {
  $('#sidebar').hide();
  map.invalidateSize();
});

function sidebarClick(id) {
  //map.addLayer(ev_mestieriLayer);
  var layer = markerClusters.getLayer(id);
  map.setView([layer.getLatLng().lat, layer.getLatLng().lng], 17);
  layer.fire("click");
  /* Hide sidebar and go to the map on small screens */
  if (document.body.clientWidth <= 767) {
    $("#sidebar").hide();
    map.invalidateSize();
  }
}

/* Basemap Layers */
var hotOSM = L.tileLayer("http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png", {
  maxZoom: 19,
  //subdomains: ["otile1", "otile2", "otile3", "otile4"],
  attribution: 'Tiles courtesy of <a href="http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png" target="_blank">Humanitarian OpenStreetMap Team</a>  Map data (c) <a href="http://www.openstreetmap.org/" target="_blank">OpenStreetMap</a> contributors, CC-BY-SA.'
});
var ggl = new L.Google();

L.mapbox.accessToken = 'pk.eyJ1IjoibW1pY290dGkiLCJhIjoiT3lGUzRyZyJ9.BaJccugbqmtNSzhB3yyr0Q';
// Replace 'examples.map-i87786ca' with your map id.
var mapboxTiles = L.tileLayer('https://{s}.tiles.mapbox.com/v4/mmicotti.c37e960f/{z}/{x}/{y}.png?access_token=' + L.mapbox.accessToken, {
    attribution: 'Tiles courtesy of <a href="http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png"> OpenStreetMap </a> trough <a href="http://www.mapbox.com/about/maps/" target="_blank"> Mapbox </a> - Style from Marco Micotti (Mapbox user)</a>'
});

/* Overlay Layers */
var highlight = L.geoJson(null);


/* Single marker cluster layer to hold all clusters */
var markerClusters = new L.MarkerClusterGroup({
  spiderfyOnMaxZoom: true,
  showCoverageOnHover: false,
  zoomToBoundsOnClick: true,
  disableClusteringAtZoom: 20
});

/* Empty layer placeholder to add to layer control for listening when to add/remove elements to markerClusters layer */
/* To add new point layer in the cluster, duplicates the following lines, up to the next related comment */
/* layer 1*/
var ev_mestieriLayer = L.geoJson(null);
var ev_mestieri      = L.geoJson(null, {
  pointToLayer: function (feature, latlng) {
    return L.marker(latlng, {
      icon: L.icon({
        iconUrl: "assets/img/mestieri.svg",
        iconSize: [30,40],
        iconAnchor: [12, 28],
        popupAnchor: [0, -25]
      }),
      title: feature.properties.nome,
      riseOnHover: true
    });
  },
  onEachFeature: function (feature, layer) {
    if (feature.properties) {
      if(feature.properties.imgdettaglio_file && feature.properties.imgcampo_file) {
        var images  = "<center><table>" + "<tr><td><img src='http://www.ecosistemaverbano.org/foto/" + feature.properties.imgdettaglio_file + "' width=\"250px\"\/></td><td><img src='http://www.ecosistemaverbano.org/foto/" + feature.properties.imgcampo_file + "' width=\"250px\"\/></td></tr>"+"<tr><td><p><small>" + feature.properties.imgdettaglio_dida + "'</small></p></td><td><p><small>" + feature.properties.imgcampo_dida + "</small></p></td></tr>"+"</table></center><br>" ;
        }
      else if (feature.properties.imgdettaglio_file) {
        var images  = "<center><table>" + "<tr><td><img src='http://www.ecosistemaverbano.org/foto/" + feature.properties.imgdettaglio_file + "' width=\"250px\"\/></td></tr>"+"<tr><td><p><small>" + feature.properties.imgdettaglio_dida + "'</small></p></td></tr>"+"</table></center><br>" ;
        }
      else {
        var images  ="";
      }
      var content = images +"<table class='table table-striped table-condensed'>" + "<tr><th>Nome</th><td>" + feature.properties.nome + "</td></tr>" + "<tr><th>Descrizione</th><td>" + feature.properties.abstract + "</td></tr>"+ "<tr><th>Tema</th><td><p>" + feature.properties.via + " - " + feature.properties.specifica_via + "</p></td></tr>"+"<tr><th>Approfondimenti</th><td> <a target='_blank' href='http://www.ecosistemaverbano.org/scheda.html?id=" + feature.properties.id + "'>Vai alla scheda di dettaglio</a></td></tr>"+"</table>";
      layer.on({
        click: function (e) {
          $("#feature-title").html(feature.properties.nome);
          $("#feature-info").html(content);          
          $("#featureModal").modal("show");
          highlight.clearLayers().addLayer(L.circleMarker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], {
            stroke: false,
            fillColor: "#C60000",
            fillOpacity: 0.7,
            radius: 5
          }));
        }
      });
      $("#feature-list tbody").append('<tr class="feature-row" id="'+L.stamp(layer)+'"><td style="vertical-align: middle;"><img width="16" height="18" src="assets/img/mestieri.svg"></td><td class="feature-name">'+layer.feature.properties.nome+'</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
      ev_mestieriSearch.push({
        name: layer.feature.properties.nome,
        //address: layer.feature.properties.descr,
        source: "ViadeiMestieri",
        id: L.stamp(layer),
        lat: layer.feature.geometry.coordinates[1],
        lng: layer.feature.geometry.coordinates[0]
      });
    }
  }
});
$.getJSON("data/ev_mestieri.geojson", function (data) {
  ev_mestieri.addData(data);
  map.addLayer(ev_mestieriLayer);
});

/* layer 2*/
var ev_acquaLayer = L.geoJson(null);
var ev_acqua = L.geoJson(null, {
  pointToLayer: function (feature, latlng) {
    return L.marker(latlng, {
      icon: L.icon({
        iconUrl: "assets/img/acqua.svg",
        iconSize: [30,40],
        iconAnchor: [12, 28],
        popupAnchor: [0, -25]
      }),
      title: feature.properties.nome,
      riseOnHover: true
    });
  },
  onEachFeature: function (feature, layer) {
    if (feature.properties) {
      if(feature.properties.imgdettaglio_file && feature.properties.imgcampo_file) {
        var images  = "<center><table>" + "<tr><td><img src='http://www.ecosistemaverbano.org/foto/" + feature.properties.imgdettaglio_file + "' width=\"250px\"\/></td><td><img src='http://www.ecosistemaverbano.org/foto/" + feature.properties.imgcampo_file + "' width=\"250px\"\/></td></tr>"+"<tr><td><p><small>" + feature.properties.imgdettaglio_dida + "'</small></p></td><td><p><small>" + feature.properties.imgcampo_dida + "</small></p></td></tr>"+"</table></center><br>" ;
        }
      else if (feature.properties.imgdettaglio_file) {
        var images  = "<center><table>" + "<tr><td><img src='http://www.ecosistemaverbano.org/foto/" + feature.properties.imgdettaglio_file + "' width=\"250px\"\/></td></tr>"+"<tr><td><p><small>" + feature.properties.imgdettaglio_dida + "'</small></p></td></tr>"+"</table></center><br>" ;
        }
      else {
        var images  ="";
      }
      var content = images +"<table class='table table-striped table-condensed'>" + "<tr><th>Nome</th><td>" + feature.properties.nome + "</td></tr>" + "<tr><th>Descrizione</th><td>" + feature.properties.abstract + "</td></tr>"+ "<tr><th>Tema</th><td><p>" + feature.properties.via + " - " + feature.properties.specifica_via + "</p></td></tr>"+"<tr><th>Approfondimenti</th><td> <a target='_blank' href='http://www.ecosistemaverbano.org/scheda.html?id=" + feature.properties.id + "'>Vai alla scheda di dettaglio</a></td></tr>"+"</table>";
      layer.on({
        click: function (e) {
          $("#feature-title").html(feature.properties.nome);
          $("#feature-info").html(content);
          $("#featureModal").modal("show");
          highlight.clearLayers().addLayer(L.circleMarker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], {
            stroke: false,
            fillColor: "#C60000",
            fillOpacity: 0.7,
            radius: 5
          }));
        }
      });
      $("#feature-list tbody").append('<tr class="feature-row" id="'+L.stamp(layer)+'"><td style="vertical-align: middle;"><img width="16" height="18" src="assets/img/acqua.svg"></td><td class="feature-name">'+layer.feature.properties.nome+'</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
      ev_acquaSearch.push({
        name: layer.feature.properties.nome,
        //address: layer.feature.properties.descr,
        source: "Viadellacqua",
        id: L.stamp(layer),
        lat: layer.feature.geometry.coordinates[1],
        lng: layer.feature.geometry.coordinates[0]
      });
    }
  }
});
$.getJSON("data/ev_acqua.geojson", function (data) {
  ev_acqua.addData(data);
  map.addLayer(ev_acquaLayer);
});


/* layer 3*/
var ev_culturaLayer = L.geoJson(null);
var ev_cultura = L.geoJson(null, {
  pointToLayer: function (feature, latlng) {
    return L.marker(latlng, {
      icon: L.icon({
        iconUrl: "assets/img/cultura.svg",
        iconSize: [30,40],
        iconAnchor: [12, 28],
        popupAnchor: [0, -25]
      }),
      title: feature.properties.nome,
      riseOnHover: true
    });
  },
  onEachFeature: function (feature, layer) {
    if (feature.properties) {
      if(feature.properties.imgdettaglio_file && feature.properties.imgcampo_file) {
        var images  = "<center><table>" + "<tr><td><img src='http://www.ecosistemaverbano.org/foto/" + feature.properties.imgdettaglio_file + "' width=\"250px\"\/></td><td><img src='http://www.ecosistemaverbano.org/foto/" + feature.properties.imgcampo_file + "' width=\"250px\"\/></td></tr>"+"<tr><td><p><small>" + feature.properties.imgdettaglio_dida + "'</small></p></td><td><p><small>" + feature.properties.imgcampo_dida + "</small></p></td></tr>"+"</table></center><br>" ;
        }
      else if (feature.properties.imgdettaglio_file) {
        var images  = "<center><table>" + "<tr><td><img src='http://www.ecosistemaverbano.org/foto/" + feature.properties.imgdettaglio_file + "' width=\"250px\"\/></td></tr>"+"<tr><td><p><small>" + feature.properties.imgdettaglio_dida + "'</small></p></td></tr>"+"</table></center><br>" ;
        }
      else {
        var images  ="";
      }
      var content = images +"<table class='table table-striped table-condensed'>" + "<tr><th>Nome</th><td>" + feature.properties.nome + "</td></tr>" + "<tr><th>Descrizione</th><td>" + feature.properties.abstract + "</td></tr>"+ "<tr><th>Tema</th><td><p>" + feature.properties.via + " - " + feature.properties.specifica_via + "</p></td></tr>"+"<tr><th>Approfondimenti</th><td> <a target='_blank' href='http://www.ecosistemaverbano.org/scheda.html?id=" + feature.properties.id + "'>Vai alla scheda di dettaglio</a></td></tr>"+"</table>";
      layer.on({
        click: function (e) {
          $("#feature-title").html(feature.properties.nome);
          $("#feature-info").html(content);
          $("#featureModal").modal("show");
          highlight.clearLayers().addLayer(L.circleMarker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], {
            stroke: false,
            fillColor: "#C60000",
            fillOpacity: 0.7,
            radius: 5
          }));
        }
      });
      $("#feature-list tbody").append('<tr class="feature-row" id="'+L.stamp(layer)+'"><td style="vertical-align: middle;"><img width="16" height="18" src="assets/img/cultura.svg"></td><td class="feature-name">'+layer.feature.properties.nome+'</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
      ev_culturaSearch.push({
        name: layer.feature.properties.nome,
        //address: layer.feature.properties.descr,
        source: "ViadellaCultura",
        id: L.stamp(layer),
        lat: layer.feature.geometry.coordinates[1],
        lng: layer.feature.geometry.coordinates[0]
      });
    }
  }
});
$.getJSON("data/ev_cultura.geojson", function (data) {
  ev_cultura.addData(data);
  map.addLayer(ev_culturaLayer);
});

/* layer 4*/
var ev_memoriaLayer = L.geoJson(null);
var ev_memoria = L.geoJson(null, {
  pointToLayer: function (feature, latlng) {
    return L.marker(latlng, {
      icon: L.icon({
        iconUrl: "assets/img/memoria.svg",
        iconSize: [30,40],
        iconAnchor: [12, 28],
        popupAnchor: [0, -25]
      }),
      title: feature.properties.nome,
      riseOnHover: true
    });
  },
  onEachFeature: function (feature, layer) {
    if (feature.properties) {
      if(feature.properties.imgdettaglio_file && feature.properties.imgcampo_file) {
        var images  = "<center><table>" + "<tr><td><img src='http://www.ecosistemaverbano.org/foto/" + feature.properties.imgdettaglio_file + "' width=\"250px\"\/></td><td><img src='http://www.ecosistemaverbano.org/foto/" + feature.properties.imgcampo_file + "' width=\"250px\"\/></td></tr>"+"<tr><td><p><small>" + feature.properties.imgdettaglio_dida + "'</small></p></td><td><p><small>" + feature.properties.imgcampo_dida + "</small></p></td></tr>"+"</table></center><br>" ;
        }
      else if (feature.properties.imgdettaglio_file) {
        var images  = "<center><table>" + "<tr><td><img src='http://www.ecosistemaverbano.org/foto/" + feature.properties.imgdettaglio_file + "' width=\"250px\"\/></td></tr>"+"<tr><td><p><small>" + feature.properties.imgdettaglio_dida + "'</small></p></td></tr>"+"</table></center><br>" ;
        }
      else {
        var images  ="";
      }
      var content = images +"<table class='table table-striped table-condensed'>" + "<tr><th>Nome</th><td>" + feature.properties.nome + "</td></tr>" + "<tr><th>Descrizione</th><td>" + feature.properties.abstract + "</td></tr>"+ "<tr><th>Tema</th><td><p>" + feature.properties.via + " - " + feature.properties.specifica_via + "</p></td></tr>"+"<tr><th>Approfondimenti</th><td> <a target='_blank' href='http://www.ecosistemaverbano.org/scheda.html?id=" + feature.properties.id + "'>Vai alla scheda di dettaglio</a></td></tr>"+"</table>";
      layer.on({
        click: function (e) {
          $("#feature-title").html(feature.properties.nome);
          $("#feature-info").html(content);
          $("#featureModal").modal("show");
          highlight.clearLayers().addLayer(L.circleMarker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], {
            stroke: false,
            fillColor: "#C60000",
            fillOpacity: 0.7,
            radius: 5
          }));
        }
      });
      $("#feature-list tbody").append('<tr class="feature-row" id="'+L.stamp(layer)+'"><td style="vertical-align: middle;"><img width="16" height="18" src="assets/img/memoria.svg"></td><td class="feature-name">'+layer.feature.properties.nome+'</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
      ev_memoriaSearch.push({
        name: layer.feature.properties.nome,
        //address: layer.feature.properties.descr,
        source: "ViadellaMemoria",
        id: L.stamp(layer),
        lat: layer.feature.geometry.coordinates[1],
        lng: layer.feature.geometry.coordinates[0]
      });
    }
  }
});
$.getJSON("data/ev_memoria.geojson", function (data) {
  ev_memoria.addData(data);
  map.addLayer(ev_memoriaLayer);
});


/*End of part to be duplicated when adding layers, see below to add layer to the cluster group*/
map = L.map("map", {
  zoom: 10,
  center: [45.9492,8.6002],
  layers: [mapboxTiles, markerClusters, highlight],
  zoomControl: false,
  attributionControl: false
});

/* Layer control listeners that allow for a single markerClusters layer */
map.on("overlayadd", function(e) {
  if (e.layer === ev_mestieriLayer) {
    markerClusters.addLayer(ev_mestieri);
  }
  if (e.layer === ev_acquaLayer) {
    markerClusters.addLayer(ev_acqua);
  }
  if (e.layer === ev_culturaLayer) {
    markerClusters.addLayer(ev_cultura);
  }
  if (e.layer === ev_memoriaLayer) {
    markerClusters.addLayer(ev_memoria);
  }
});

map.on("overlayremove", function(e) {
  if (e.layer === ev_mestieriLayer) {
    markerClusters.removeLayer(ev_mestieri);
  }
  if (e.layer === ev_acquaLayer) {
    markerClusters.removeLayer(ev_acqua);
  }
  if (e.layer === ev_culturaLayer) {
    markerClusters.removeLayer(ev_cultura);
  }
  if (e.layer === ev_memoriaLayer) {
    markerClusters.removeLayer(ev_memoria);
  }
});

/* Clear feature highlight when map is clicked */
map.on("click", function(e) {
  highlight.clearLayers();
});

/* Attribution control */
function updateAttribution(e) {
  $.each(map._layers, function(index, layer) {
    if (layer.getAttribution) {
      $("#attribution").html((layer.getAttribution()));
    }
  });
}
map.on("layeradd", updateAttribution);
map.on("layerremove", updateAttribution);

var attributionControl = L.control({
  position: "bottomright"
});
attributionControl.onAdd = function (map) {
  var div = L.DomUtil.create("div", "leaflet-control-attribution");
  div.innerHTML = "</span><a href='#' onclick='$(\"#attributionModal\").modal(\"show\"); return false;'>Credits</a>";
  return div;
};
map.addControl(attributionControl);

var zoomControl = L.control.zoom({
  position: "bottomright"
}).addTo(map);

/* GPS enabled geolocation control set to follow the user's location */
var locateControl = L.control.locate({
  position: "bottomright",
  drawCircle: true,
  follow: true,
  setView: true,
  keepCurrentZoomLevel: true,
  markerStyle: {
    weight: 1,
    opacity: 0.8,
    fillOpacity: 0.8
  },
  circleStyle: {
    weight: 1,
    clickable: false
  },
  icon: "icon-direction",
  metric: false,
  strings: {
    title: "My location",
    popup: "You are within {distance} {unit} from this point",
    outsideMapBoundsMsg: "You seem located outside the boundaries of the map"
  },
  locateOptions: {
    maxZoom: 18,
    watch: true,
    enableHighAccuracy: true,
    maximumAge: 10000,
    timeout: 10000
  }
}).addTo(map);

/* Larger screens get expanded layer control and visible sidebar */
if (document.body.clientWidth <= 767) {
  var isCollapsed = true;
} else {
  var isCollapsed = false;
}

var baseLayers = {
  "OSM - Ecosistema Verbano basemap": mapboxTiles,
  "Google Aerial Imagery": ggl,
  //"Imagery with Streets": mapquestHYB
};

var groupedOverlays = {
  "Vie di Ecosistema Verbano": {
    "<img src='assets/img/mestieri.svg' width='24' height='28'>&nbsp;Via dei Mestieri": ev_mestieriLayer,
    "<img src='assets/img/acqua.svg' width='24' height='28'>&nbsp;Via dell'acqua": ev_acquaLayer,
    "<img src='assets/img/cultura.svg' width='24' height='28'>&nbsp;Via della Cultura": ev_culturaLayer,
    "<img src='assets/img/memoria.svg' width='24' height='28'>&nbsp;Via della Memoria": ev_memoriaLayer
    },
  "Reference": {

  }
};

var layerControl = L.control.groupedLayers(baseLayers, groupedOverlays, {
  collapsed: isCollapsed
}).addTo(map);

/* Highlight search box text on click */
$("#searchbox").click(function () {
  $(this).select();
});

/* Typeahead search functionality */
$(document).one("ajaxStop", function () {
  $("#loading").hide();
  /* Fit map to boroughs bounds */
  map.fitBounds(ev_cultura.getBounds());
  featureList = new List("features", {valueNames: ["feature-name"]});
  featureList.sort("feature-name", {order:"asc"});

  var ev_mestieriBH = new Bloodhound({
    name: "ViadeiMestieri",
    datumTokenizer: function (d) {
      return Bloodhound.tokenizers.whitespace(d.name);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: ev_mestieriSearch,
    limit: 10
  });
  
  var ev_acquaBH = new Bloodhound({
    name: "Viadellacqua",
    datumTokenizer: function (d) {
      return Bloodhound.tokenizers.whitespace(d.name);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: ev_acquaSearch,
    limit: 10
  });
  
  var ev_culturaBH = new Bloodhound({
    name: "ViadellaCultura",
    datumTokenizer: function (d) {
      return Bloodhound.tokenizers.whitespace(d.name);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: ev_culturaSearch,
    limit: 10
  });
  
  var ev_memoriaBH = new Bloodhound({
    name: "ViadellaMemoria",
    datumTokenizer: function (d) {
      return Bloodhound.tokenizers.whitespace(d.name);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: ev_memoriaSearch,
    limit: 10
  });

  var geonamesBH = new Bloodhound({
    name: "GeoNames",
    datumTokenizer: function (d) {
      return Bloodhound.tokenizers.whitespace(d.name);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: "http://api.geonames.org/searchJSON?username=bootleaf&featureClass=P&maxRows=5&countryCode=US&name_startsWith=%QUERY",
      filter: function (data) {
        return $.map(data.geonames, function (result) {
          return {
            name: result.name + ", " + result.adminCode1,
            lat: result.lat,
            lng: result.lng,
            source: "GeoNames"
          };
        });
      },
      ajax: {
        beforeSend: function (jqXhr, settings) {
          settings.url += "&east=" + map.getBounds().getEast() + "&west=" + map.getBounds().getWest() + "&north=" + map.getBounds().getNorth() + "&south=" + map.getBounds().getSouth();
          $("#searchicon").removeClass("fa-search").addClass("fa-refresh fa-spin");
        },
        complete: function (jqXHR, status) {
          $('#searchicon').removeClass("fa-refresh fa-spin").addClass("fa-search");
        }
      }
    },
    limit: 10
  });
  ev_mestieriBH.initialize();
  ev_acquaBH.initialize();
  ev_culturaBH.initialize();
  ev_memoriaBH.initialize();
  //museumsBH.initialize();
  geonamesBH.initialize();

  /* instantiate the typeahead UI */
  $("#searchbox").typeahead({
    minLength: 3,
    highlight: true,
    hint: false
  }, {
    name: "ViadeiMestieri",
    displayKey: "Name",
    source: ev_mestieriBH.ttAdapter(),
    templates: {
      header: "<h4 class='typeahead-header'><img src='assets/img/mestieri.svg' width='24' height='28'>&nbsp;Via dei Mestieri</h4>",
      suggestion: Handlebars.compile(["{{name}}<br>&nbsp;<small>{{address}}</small>"].join(""))
    }
  },{
    name: "Viadellacqua",
    displayKey: "Name",
    source: ev_acquaBH.ttAdapter(),
    templates: {
      header: "<h4 class='typeahead-header'><img src='assets/img/acqua.svg' width='24' height='28'>&nbsp;Via dell'acqua</h4>",
      suggestion: Handlebars.compile(["{{name}}<br>&nbsp;<small>{{address}}</small>"].join(""))
    }
  },{
    name: "ViadellaCultura",
    displayKey: "Name",
    source: ev_culturaBH.ttAdapter(),
    templates: {
      header: "<h4 class='typeahead-header'><img src='assets/img/cultura.svg' width='24' height='28'>&nbsp;Via della Cultura</h4>",
      suggestion: Handlebars.compile(["{{name}}<br>&nbsp;<small>{{address}}</small>"].join(""))
    }
  },{
    name: "ViadellaMemoria",
    displayKey: "Name",
    source: ev_memoriaBH.ttAdapter(),
    templates: {
      header: "<h4 class='typeahead-header'><img src='assets/img/memoria.svg' width='24' height='28'>&nbsp;Via della Memoria</h4>",
      suggestion: Handlebars.compile(["{{name}}<br>&nbsp;<small>{{address}}</small>"].join(""))
    }
  },{
    name: "GeoNames",
    displayKey: "name",
    source: geonamesBH.ttAdapter(),
    templates: {
      header: "<h4 class='typeahead-header'><img src='assets/img/globe.png' width='25' height='25'>&nbsp;GeoNames</h4>"
    }
  }).on("typeahead:selected", function (obj, datum) {
    if (datum.source === "ViadeiMestieri") {
      if (!map.hasLayer(ev_mestieriLayer)) {
        map.addLayer(ev_mestieriLayer);
      }
      map.setView([datum.lat, datum.lng], 17);
      if (map._layers[datum.id]) {
        map._layers[datum.id].fire("click");
      }
    }
    if (datum.source === "Viadellacqua") {
      if (!map.hasLayer(ev_acquaLayer)) {
        map.addLayer(ev_acquaLayer);
      }
      map.setView([datum.lat, datum.lng], 17);
      if (map._layers[datum.id]) {
        map._layers[datum.id].fire("click");
      }
    }
    if (datum.source === "ViadellaCultura") {
      if (!map.hasLayer(ev_culturaLayer)) {
        map.addLayer(ev_culturaLayer);
      }
      map.setView([datum.lat, datum.lng], 17);
      if (map._layers[datum.id]) {
        map._layers[datum.id].fire("click");
      }
    }
    if (datum.source === "ViadellaMemoria") {
      if (!map.hasLayer(ev_memoriaLayer)) {
        map.addLayer(ev_memoriaLayer);
      }
      map.setView([datum.lat, datum.lng], 17);
      if (map._layers[datum.id]) {
        map._layers[datum.id].fire("click");
      }
    }
    if (datum.source === "GeoNames") {
      map.setView([datum.lat, datum.lng], 14);
    }
    if ($(".navbar-collapse").height() > 50) {
      $(".navbar-collapse").collapse("hide");
    }
  }).on("typeahead:opened", function () {
    $(".navbar-collapse.in").css("max-height", $(document).height() - $(".navbar-header").height());
    $(".navbar-collapse.in").css("height", $(document).height() - $(".navbar-header").height());
  }).on("typeahead:closed", function () {
    $(".navbar-collapse.in").css("max-height", "");
    $(".navbar-collapse.in").css("height", "");
  });
  $(".twitter-typeahead").css("position", "static");
  $(".twitter-typeahead").css("display", "block");
});
